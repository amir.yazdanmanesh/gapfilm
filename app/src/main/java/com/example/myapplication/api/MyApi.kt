package com.example.myapplication.api

import com.example.myapplication.models.myrequest
import com.example.myapplication.responses.MoviesResponse

import org.json.JSONObject
import retrofit2.http.*


interface MyApi {



    @Headers( "Content-Type: application/json" )
    @POST("GetContentList")
    suspend fun getMoviesList(
        @Body request: String
    ): MoviesResponse
}