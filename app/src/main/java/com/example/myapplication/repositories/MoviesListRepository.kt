package com.example.myapplication.repositories

import androidx.paging.*
import com.example.myapplication.repositories.paging.PagingMovies
import com.example.myapplication.api.MyApi



import javax.inject.Inject

class MoviesListRepository @Inject constructor(
    private val myApi: MyApi,
)  {




    fun getRequest() =
        Pager(
            config = PagingConfig(
                pageSize = 20,
                maxSize = 100,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { PagingMovies(myApi) }
        ).flow

}