package com.example.myapplication.repositories.paging

import androidx.paging.PagingSource
import com.bumptech.glide.load.HttpException
import com.example.myapplication.responses.GetContent
import com.example.myapplication.api.MyApi

import org.json.JSONObject
import java.io.IOException


class PagingMovies(
    private val api: MyApi


) : PagingSource<Int, GetContent>() {
    val pageIndex = 1
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, GetContent> {
        val position = params.key ?: pageIndex

        return try {
            val jsonObjRequest = JSONObject()

            val jsonAdd = JSONObject() // we need another object to store the address

            jsonAdd.put("RequestType", 2)

            jsonAdd.put("RequestId",null)
            jsonAdd.put("PageSize", 20)
            jsonAdd.put("PageIndex", position)
            jsonAdd.put("OrderBy", "createdate")
            jsonAdd.put("Order", "desc")

            jsonObjRequest.put("request",jsonAdd)


            val response = api.getMoviesList(jsonObjRequest.toString()).Result.GetContentList

            LoadResult.Page(
                data = response,
                prevKey = if (position == pageIndex) null else position - 1,
                nextKey = if (response.isEmpty()) null else position + 1
            )
        } catch (exception: IOException) {
            LoadResult.Error(exception)
        } catch (exception: HttpException) {
            LoadResult.Error(exception)
        }
    }
}