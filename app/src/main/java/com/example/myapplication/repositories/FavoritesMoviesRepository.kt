package com.radmantech.a4030.repositories

import com.example.myapplication.db.MyDao
import com.example.myapplication.db.tables.TblContent

import javax.inject.Inject

class FavoritesMoviesRepository @Inject constructor(
    private val myDao: MyDao
)  {



    // DB
    suspend fun insertContent(tblContent: TblContent) =
        myDao.insertContent(tblContent)

    fun getAllContent() = myDao.getAllContent()

     fun isFave(id:Int) = myDao.getIsFave(id)
    suspend fun deleteContent(id:Int) = myDao.deleteContent(id)

}