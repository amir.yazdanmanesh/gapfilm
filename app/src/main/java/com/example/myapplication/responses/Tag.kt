package com.example.myapplication.responses

data class Tag(
    val BackgroundImage: String,
    val Description: Any,
    val Image: String,
    val IsFollowed: Boolean,
    val IsSelected: Boolean,
    val Name: String,
    val SectionPriority: Int,
    val TagId: Int
)