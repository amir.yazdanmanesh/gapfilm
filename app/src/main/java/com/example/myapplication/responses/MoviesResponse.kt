package com.example.myapplication.responses


data class MoviesResponse(
    val Message: String,
    val Result: Result,
    val Status: Int
)


data class Result(
    val GetContentList: List<GetContent>,
    val TotalPages: Int
)
