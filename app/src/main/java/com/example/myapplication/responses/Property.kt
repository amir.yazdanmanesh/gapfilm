package com.example.myapplication.responses

data class Property(
    val Name: String,
    val PropertyId: Int,
    val Value: String
)