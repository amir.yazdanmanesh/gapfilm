package com.example.myapplication.utils


object Constants {

    const val DATABASE_NAME = "movies_DB"
    const val BASE_URL = "https://core.gapfilm.ir/mobile/request.asmx/"


    const val SHARED_PREFERENCES_NAME = "sharedPref"
    const val IS_FIRST_TIME_TO_GET_PROVINCE = "IS_FIRST_TIME_TO_GET_PROVINCE"
    const val USER_TYPE_CATEGORY_SIZE = "USER_TYPE_CATEGORY_SIZE"
    const val IS_CATEGORIES_ADDED_TO_DB = "IS_CATEGORIES_ADDED_TO_DB"
    const val IS_USER_LOGIN = "IS_USER_LOGIN"
    const val USER_IMAGE_PROFILE = "USER_IMAGE_PROFILE"

    const val NETWORK_FAILURE = "خطا در دسترسی به اینترنت"


}