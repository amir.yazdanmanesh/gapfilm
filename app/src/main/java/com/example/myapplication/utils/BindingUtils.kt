package com.example.myapplication.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.myapplication.R

@BindingAdapter("ThumbImage")
fun loadImage(view: ImageView, url: String?) {
    Glide.with(view)
        .load(url)
        .apply( RequestOptions().placeholder(R.drawable.placeholder).error(R.drawable.placeholder))
        .into(view)
}