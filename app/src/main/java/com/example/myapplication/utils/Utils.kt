package com.example.myapplication.utils

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Build
import android.view.Window
import android.view.WindowManager
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import com.example.myapplication.interfaces.DialogListener

import java.text.SimpleDateFormat
import java.util.*


object Utils {

    fun getCurrentTime(): String {
        val df: java.text.DateFormat = SimpleDateFormat("HH:mm")
        return df.format(Calendar.getInstance().time)
    }

    fun convertFullDateToTimeMessage(date: String): String {
        return date.substring(11, 16)
    }

    fun convertFullDateToTimeMessage(date: String, i: Int): String {
        return date.substring(i)
    }

    val BASIC_LISTENER: DialogListener = object : DialogListener {
        override fun OnAccept(dialog: Dialog) {
            dialog.dismiss()
        }

        override fun OnDecline(dialog: Dialog) {
            dialog.dismiss()
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun initStatusBar(color: Int, activity: Activity) {
        val window: Window = activity.window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(activity.applicationContext, color)
    }


    private var density = 1f

    fun dp(value: Float, context: Context): Int {
        if (density == 1f) {
            checkDisplaySize(context)
        }
        return if (value == 0f) {
            0
        } else Math.ceil((density * value).toDouble()).toInt()
    }


    private fun checkDisplaySize(context: Context) {
        try {
            density = context.resources.displayMetrics.density
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun splitStringWithComma(string: String): Array<String> {
        return string.split(",").toTypedArray()
    }

}