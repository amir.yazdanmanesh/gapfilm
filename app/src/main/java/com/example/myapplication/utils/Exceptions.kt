package com.example.myapplication.utils

import java.io.IOException

class NoInternetExceptions(message: String): IOException(message)