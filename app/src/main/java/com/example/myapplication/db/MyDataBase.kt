package com.example.myapplication.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.myapplication.db.tables.TblContent


@Database(
    entities = [
        TblContent::class

    ],
    version = 3
)
abstract class MyDataBase : RoomDatabase() {

    abstract fun getMyDao(): MyDao

}
