package com.example.myapplication.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.myapplication.db.tables.TblContent


@Dao
interface MyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertContent(tblContent: TblContent): Long

    @Query("SELECT * FROM Get_Content")
    fun getAllContent(): LiveData<List<TblContent>>

    @Query("SELECT * FROM Get_Content where ContentID=:id")
     fun getIsFave(id: Int): LiveData<List<TblContent>>

   @Query("delete FROM Get_Content where ContentID=:id")
   suspend fun deleteContent(id: Int)


}