package com.example.myapplication.db.tables

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "Get_Content")
data class TblContent(

    @PrimaryKey(autoGenerate = false)
    val ContentID: Int,

    val LandscapeImage: String,

    val Summary: String,
    val ThumbImage: String,
    val Title: String,

    val ZoneID: Int
)