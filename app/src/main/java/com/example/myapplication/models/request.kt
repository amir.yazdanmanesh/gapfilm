package com.example.myapplication.models

data class request (
    val RequestType: Int,
    val RequestId: String,
    val PageSize: Int,
    val PageIndex: Int,
    val OrderBy: String,
    val Order: String

)