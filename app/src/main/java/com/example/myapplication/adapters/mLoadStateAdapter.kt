package com.example.myapplication.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R

import kotlinx.android.synthetic.main.load_state_footer.view.*

class mLoadStateAdapter(private val retry: () -> Unit) :
    LoadStateAdapter<mLoadStateAdapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): Holder {


        return Holder(   LayoutInflater.from(parent.context)
            .inflate(R.layout.load_state_footer, parent, false))
    }

    override fun onBindViewHolder(holder: Holder, loadState: LoadState) {
              holder.itemView.btnRetryFooter.setOnClickListener {

                  retry.invoke()
              }


        holder.itemView.progressBarRetry.isVisible = loadState is LoadState.Loading
        holder.itemView.btnRetryFooter.isVisible = loadState !is LoadState.Loading
        holder.itemView.txtRetryError.isVisible = loadState !is LoadState.Loading
    }

    inner class Holder(itemView: View):RecyclerView.ViewHolder(itemView)


}