package com.example.myapplication.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.databinding.MainFragmentItemBinding
import com.example.myapplication.responses.GetContent
import kotlinx.android.synthetic.main.main_fragment_item.view.*


class MainAdapter(

) : PagingDataAdapter<GetContent, MainAdapter.MoviesViewHolder>(differCallBack){

    inner class MoviesViewHolder(
        var recyclerviewMovieBinding: MainFragmentItemBinding
    ) : RecyclerView.ViewHolder(recyclerviewMovieBinding.root)

    var con:Context?=null
    companion object {

        private val differCallBack = object : DiffUtil.ItemCallback<GetContent>(){
        override fun areItemsTheSame(
            oldItem: GetContent,
            newItem: GetContent
        ): Boolean {
            return oldItem.ContentID==newItem.ContentID
        }

        override fun areContentsTheSame(
            oldItem: GetContent,
            newItem: GetContent
        ): Boolean {
            return oldItem==newItem
        }
    }
    }
    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesViewHolder {

        con=parent.context
        return MoviesViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.main_fragment_item,
                parent,
                false
            )
        )
    }
    val differ = AsyncListDiffer(this,differCallBack)

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {
        val currentMovie = getItem(position)
        holder.itemView.apply {

                      setOnClickListener {
                onItemClickListener?.let {
                    if (currentMovie != null) {
                        it(currentMovie)
                    }
                }
            }
        }
        holder.itemView.apply {
            val title = currentMovie!!.Title
            val typeInt = currentMovie.ZoneID
            var typeString:String
            typeString = if (typeInt==3)
                "سریالی"
            else
                "سینمایی"


            holder.recyclerviewMovieBinding.movie = currentMovie

            holder.recyclerviewMovieBinding.textTitle.text = title
            holder.recyclerviewMovieBinding.textType.text = typeString



        }
    }


    private var onItemClickListener: ((GetContent) -> Unit)? = null

    fun setOnItemClickListener(listener: (GetContent) -> Unit) {
        onItemClickListener = listener
    }


    interface OnSpecificRequestListener {
        fun deleteRequest(_id: Long)
    }
}