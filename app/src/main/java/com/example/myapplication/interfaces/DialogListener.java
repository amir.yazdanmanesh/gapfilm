package com.example.myapplication.interfaces;

import android.app.Dialog;

public interface DialogListener {
    void OnAccept(Dialog dialog);
    void OnDecline(Dialog dialog);
}
