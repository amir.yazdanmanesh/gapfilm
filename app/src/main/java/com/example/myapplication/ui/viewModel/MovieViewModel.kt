package com.example.myapplication.ui.viewModel

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.myapplication.api.MyApi
import com.example.myapplication.repositories.MoviesListRepository
import com.example.myapplication.repositories.paging.PagingMovies
import com.example.myapplication.responses.GetContent
import com.example.myapplication.responses.MoviesResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import retrofit2.Response

class MovieViewModel @ViewModelInject constructor(
    private val repository: MoviesListRepository,
    private val api: MyApi
) : ViewModel() {


    private val currentCategory: MutableLiveData<String> = MutableLiveData()


    val movies: Flow<PagingData<GetContent>> = repository.getRequest().cachedIn(viewModelScope)

}