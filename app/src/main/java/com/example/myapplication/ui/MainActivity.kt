package com.example.myapplication.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.get
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.myapplication.R
import com.google.android.material.shape.CornerFamily
import com.google.android.material.shape.MaterialShapeDrawable
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setupWithNavController(nav_host_fragment.findNavController())
        val radius = resources.getDimension(R.dimen.radius_small)
        val bottomNavigationViewBackground =
            bottomNavigationView.background as MaterialShapeDrawable
        bottomNavigationViewBackground.shapeAppearanceModel =
            bottomNavigationViewBackground.shapeAppearanceModel.toBuilder()
                .setTopRightCorner(CornerFamily.ROUNDED, radius)
                .setTopLeftCorner(CornerFamily.ROUNDED, radius)
                .build()
        setOnMenuItemClickListenerToBottomNavigationView();
    }

    fun setOnMenuItemClickListenerToBottomNavigationView() {
        bottomNavigationView.menu[1].setOnMenuItemClickListener {
            nav_host_fragment.findNavController().navigate(
                R.id.action_global_mainMovieFragment
            )

            true
        }
        bottomNavigationView.menu[0].setOnMenuItemClickListener {
            nav_host_fragment.findNavController().navigate(
                R.id.action_global_favoritesMovieFragment
            )
            true
        }

    }


}