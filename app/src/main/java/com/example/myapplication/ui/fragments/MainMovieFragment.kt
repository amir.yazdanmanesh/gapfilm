package com.example.myapplication.ui.fragments

import android.os.Bundle
import android.view.View
import androidx.annotation.Nullable
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.ActivityNavigator
import androidx.navigation.NavDestination
import androidx.navigation.NavOptions
import androidx.navigation.Navigator
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import com.example.myapplication.R
import com.example.myapplication.adapters.MainAdapter
import com.example.myapplication.adapters.mLoadStateAdapter
import com.example.myapplication.ui.viewModel.MovieViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.main_fragment.*
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch


@AndroidEntryPoint
class MainMovieFragment : Fragment(R.layout.main_fragment) {
    lateinit var myAdapter: MainAdapter


    private val viewModel: MovieViewModel by viewModels()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        getDataFromServer()



    }


    fun getDataFromServer(){
     lifecycleScope.launch {
            viewModel.movies.collectLatest {

                myAdapter = MainAdapter()
                myAdapter.submitData(viewLifecycleOwner.lifecycle, it)
                rc_Movies.setHasFixedSize(true)


                myAdapter.setOnItemClickListener {

                    val bundle = bundleOf(
                        "imageUrl" to it.LandscapeImage,
                        "Summary" to it.Summary,
                        "Title" to it.Title,
                        "ThumbImage" to it.ThumbImage,
                        "ContentID" to it.ContentID,

                        "ZoneID" to it.ZoneID


                    )

                    findNavController().navigate(
                        R.id.action_mainMovieFragment_to_detailMovieFragment, bundle
                    )

                }
                rc_Movies.adapter = myAdapter.withLoadStateHeaderAndFooter(
                    header = mLoadStateAdapter { myAdapter.retry() },
                    footer = mLoadStateAdapter { myAdapter.retry() },
                )

                myAdapter.addLoadStateListener { loadState ->

                    progressbarCallVideo.isVisible = loadState.source.refresh is LoadState.Loading
                    rc_Movies.isVisible = loadState.source.refresh is LoadState.NotLoading
                    button_retry.isVisible = loadState.source.refresh is LoadState.Error
                    text_view_error.isVisible = loadState.source.refresh is LoadState.Error

                    // empty view
                    if (loadState.source.refresh is LoadState.NotLoading &&
                        loadState.append.endOfPaginationReached &&
                        myAdapter.itemCount < 1
                    ) {
                        rc_Movies.isVisible = false
                        text_view_empty.isVisible = true
                        Img_request_null.isVisible = true


                    } else {
                        text_view_empty.isVisible = false
                        Img_request_null.isVisible = false
                    }


                }


            }
        }



    }

}
