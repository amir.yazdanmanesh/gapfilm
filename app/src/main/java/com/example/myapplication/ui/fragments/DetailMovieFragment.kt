package com.example.myapplication.ui.fragments

import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.myapplication.R
import com.example.myapplication.db.tables.TblContent
import com.example.myapplication.ui.viewModel.FavoritesMovieViewModel
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailMovieFragment : Fragment(R.layout.detail_fragment) {

    private val viewModel: FavoritesMovieViewModel by viewModels()
             var isFave:Boolean = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val title =arguments?.getString("Title")as String
        val imageUrl =arguments?.getString("imageUrl") as String
        val Summary =arguments?.getString("Summary")as String

        val ThumbImage =arguments?.getString("ThumbImage")as String
        val ZoneID =arguments?.getInt("ZoneID")as Int
        val ContentID =arguments?.getInt("ContentID") as Int



        val toolbar: Toolbar = view.findViewById(R.id.toolbar)
        val wv = view.findViewById(R.id.webwiew) as WebView
        val faveBtn = view.findViewById(R.id.btn_favorite) as FloatingActionButton

        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {
            findNavController().popBackStack()
        }
        val collapsingToolbar: CollapsingToolbarLayout = view.findViewById(R.id.collapsing_toolbar)
        collapsingToolbar.title = title

        loadImage(view, imageUrl)


        val main_text = "<html>" +
                "<head></head>" +
                "<body dir='rtl' style='font-size: " + 16 +
                "px; text-align: justify; max-lines: 5;' >" +
                Summary +
                "</body>" +
                "</html>"
        wv.loadData(main_text, "text/html", "UTF-8")


        faveBtn.setOnClickListener {
                if (isFave){
                    viewModel.deleteContent(
                        ContentID
                    )

                    faveBtn.setImageResource(R.drawable.ic_heart_white);
                    isFave=false
                }else{
                    viewModel.insertContent(
                        TblContent(
                            ContentID,
                            imageUrl,
                            Summary,
                            ThumbImage,
                            title  ,
                            ZoneID
                        )
                    )

                    faveBtn.setImageResource(R.drawable.ic_heart_filed);
                    isFave=true

                }


        }

        checkIsFave(ContentID,faveBtn)
    }

    private fun checkIsFave(id: Int,fab:FloatingActionButton) {

        viewModel.isFave(id).observe(viewLifecycleOwner, {
            if (it.isNotEmpty())
            {
                fab.setImageResource(R.drawable.ic_heart_filed);
                isFave=true
            }


        })


    }

    private fun loadImage(v: View, url: String?) {
        val imageView: ImageView = v.findViewById(R.id.backdrop)
        Glide.with(imageView)
            .load(url)
            .apply( RequestOptions().placeholder(R.drawable.placeholder).error(R.drawable.placeholder))
            .into(imageView)
    }



}
