package com.example.myapplication.ui.viewModel

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.myapplication.db.tables.TblContent
import com.example.myapplication.repositories.MoviesListRepository
import com.example.myapplication.responses.GetContent
import com.example.myapplication.responses.MoviesResponse
import com.radmantech.a4030.repositories.FavoritesMoviesRepository
import kotlinx.coroutines.launch
import retrofit2.Response

class FavoritesMovieViewModel @ViewModelInject constructor(
    private val repository: FavoritesMoviesRepository
) : ViewModel() {

    // DB
     fun insertContent(tblContent: TblContent) = viewModelScope.launch {
        repository.insertContent(tblContent)
    }
    fun getgetAllContent() = repository.getAllContent()

    fun isFave(id:Int)=repository.isFave(id)

    fun deleteContent(id:Int) {
        viewModelScope.launch {
            repository.deleteContent(id)
        }
    }


}