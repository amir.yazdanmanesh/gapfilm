package com.example.myapplication.ui.fragments

import android.os.Bundle
import android.os.Parcelable
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.paging.map
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.adapters.FavoriteAdapter
import com.example.myapplication.adapters.MainAdapter
import com.example.myapplication.adapters.mLoadStateAdapter
import com.example.myapplication.ui.viewModel.FavoritesMovieViewModel
import com.example.myapplication.ui.viewModel.MovieViewModel

import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.main_fragment.*

@AndroidEntryPoint
class FavoritesMovieFragment : Fragment(R.layout.favorite_fragment) {
    lateinit var myAdapter: FavoriteAdapter


    private val viewModel: FavoritesMovieViewModel by viewModels()
    lateinit var mRec:RecyclerView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mRec=view.findViewById(R.id.rc_Movies)
        myAdapter = FavoriteAdapter()

        setUpRecyclerView()

        viewModel.getgetAllContent().observe(viewLifecycleOwner, {
            if (it.isEmpty()){
                text_view_empty.visibility=View.VISIBLE
            }else
            myAdapter.differ.submitList(it)



        })


        myAdapter.setOnItemClickListener {

            val bundle = bundleOf(
                "imageUrl" to it.LandscapeImage,
                "Summary" to it.Summary,
                "Title" to it.Title,
                "ThumbImage" to it.ThumbImage,
                "ContentID" to it.ContentID,
                "ZoneID" to it.ZoneID


            )

            findNavController().navigate(
                R.id.action_favoritesMovieFragment_to_detailMovieFragment,bundle
            )

        }

    }
    private fun setUpRecyclerView() {

        mRec.apply {

            adapter = myAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }

    }




}
